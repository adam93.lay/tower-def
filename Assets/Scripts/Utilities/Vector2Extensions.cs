﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
  public static class Vector2Extensions
  {
    public static Quaternion Face(this Vector2 pos, Vector2 target, float offset = 0)
    {
      Vector3 vectorToTarget = target - pos;
      vectorToTarget.z = 0;

      //Rotate to follow target
      float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg + offset;

      return Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public static Vector2 MoveThroughBezierTo(this Vector2 from, Vector2 midpoint, Vector2 to, float time)
    {
      Vector2 ab = Vector2.Lerp(from, midpoint, time);
      Vector2 bc = Vector2.Lerp(midpoint, to, time);
      return Vector2.Lerp(ab, bc, time);
    }
  }
}
