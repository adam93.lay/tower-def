﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Utilities
{
  public static class EnumHelper
  {
    public static T Parse<T>(string type) where T : struct, IConvertible
    {
      return (T)Enum.Parse(typeof(T), type);
    }
  }
}
