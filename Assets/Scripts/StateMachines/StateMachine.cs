﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.StateMachines
{
  public class StateMachine<T>
  {
    public T Owner { get; private set; }
    public State<T> CurrentState { get; private set; }

    public StateMachine(T owner)
    {
      Owner = owner;
    }

    public void ChangeState(State<T> newState)
    {
      CurrentState?.ExitState(Owner);
      CurrentState = newState;
      CurrentState.EnterState(Owner);
    }

    public void Update()
    {
      CurrentState?.UpdateState(Owner);
    }
  }
}
