﻿namespace Assets.Scripts.StateMachines
{
  public abstract class State<T>
  {
    public virtual void EnterState(T owner) { }
    public virtual void ExitState(T owner) { }
    public virtual void UpdateState(T owner) { }
  }
}
