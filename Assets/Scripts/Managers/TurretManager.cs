﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Assets.Scripts.Data;
using Assets.Scripts.Managers;
using Assets.Scripts.Turrets;
using Assets.Scripts.Utilities;
using Pathfinding.Astar;
using UnityEngine;

public class TurretManager : MonoBehaviour
{
  public GameObject TurretPrefab;

  private List<GameObject> _turrets;
  private Grid _grid;

  //private ITurret[] _turretPrototypes;
  private ScoreManager _scoreManager;

  private Dictionary<string, TurretConfig> _turretConfigs;
  private InterfaceManager _interfaceManager;

  private void Start()
  {
    _scoreManager = FindObjectOfType<ScoreManager>();
    _interfaceManager = FindObjectOfType<InterfaceManager>();

    _turrets = new List<GameObject>();
    _grid = FindObjectOfType<Grid>();

    _turretConfigs = Resources.LoadAll<TurretConfig>("Config/Turrets")
      .ToDictionary(k => k.name, v => v);
    
    MouseManager.OnPositionSelected += OnPositionSelected;
  }

  private void OnPositionSelected(Vector2 pos)
  {
    GameObject turret = GetTurret(pos);

    if (turret == null)
      return;

    // TODO: Some kind of upgrade panel or idk
  }

  public GameObject GetTurret(Vector2 pos)
  {
    GameObject turret = _turrets.FirstOrDefault(t => (Vector2)t.transform.position == pos);

    return turret;
  }

  public void Upgrade()
  {
    GameObject turretGo = GetTurret(MouseManager.SelectedPosition.Value);

    if (turretGo == null)
      return;

    var turret = turretGo.GetComponentInChildren<Turret>();

    if (_scoreManager.SpendMoney(turret.Config.Cost))
      turret.Upgrade();
    else
      _interfaceManager.ShowMessage("Not enough money!");
  }

  public void Sell()
  {
    GameObject turret = GetTurret(MouseManager.SelectedPosition.Value);

    if (turret == null)
      return;

    // TODO: Turret cost
    int turretCost = 10;

    _scoreManager.AddMoney(turretCost / 2);

    _turrets.Remove(turret);
    Destroy(turret);
    _grid.CreateGrid();
  }

  public TurretConfig GetTurretConfig(string name)
  {
    return _turretConfigs[name];
  }
  
  public void SpawnTurret(TurretConfig config, Vector2 position)
  {
    GameObject go = Instantiate(TurretPrefab);

    go.name = config.Name;
    go.GetComponent<Turret>().Config = config;
    go.transform.SetParent(this.transform);
    go.transform.position = position;

    _turrets.Add(go);

    _grid.CreateGrid();
  }
}
