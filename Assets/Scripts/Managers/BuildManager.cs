﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Data;
using Assets.Scripts.Pathfinding;
using Assets.Scripts.Turrets;
using Assets.Scripts.Utilities;
using Pathfinding.Astar;
using UnityEngine;

namespace Assets.Scripts.Managers
{
  public class BuildManager : MonoBehaviour
  {
    private Pathfinder _pathfinder;
    private TurretManager _turretManager;
    private EnemyManager _enemyManager;
    private ScoreManager _scoreManager;
    private InterfaceManager _interfaceManager;

    private void Start()
    {
      _pathfinder = FindObjectOfType<Pathfinder>();
      _turretManager = FindObjectOfType<TurretManager>();
      _enemyManager = FindObjectOfType<EnemyManager>();
      _scoreManager = FindObjectOfType<ScoreManager>();
      _interfaceManager = FindObjectOfType<InterfaceManager>();
    }

    public void BuildTurret(string type)
    {
      Vector2? position = MouseManager.SelectedPosition;

      if (!position.HasValue || !IsPlacementValid(position.Value))
      {
        _interfaceManager.ShowMessage("Can't place a turret here!");
        return;
      }

      TurretConfig config= _turretManager.GetTurretConfig(type);

      if (_scoreManager.SpendMoney(config.Cost))
        _turretManager.SpawnTurret(config, position.Value);
      else
        _interfaceManager.ShowMessage("Not enough money!");
    }
    
    private bool IsPlacementValid(Vector2 position)
    {
      // Check for collisions with existing objects
      RaycastHit2D hit = Physics2D.BoxCast(position, Vector2.one * 0.95f, 0, Vector2.zero);

      // Not valid if something is in the way (probably turret or enemy)
      if (hit)
        return false;

      // Attempt to find a valid path from Spawn to Destination
      // If there's no valid path then this turret would be blocking the enemies
      PathResult pathResult = _pathfinder.FindPath(
        _enemyManager.Spawn.transform.position,
        _enemyManager.Destination.transform.position,
        position);

      return pathResult.Success;
    }
  }
}
