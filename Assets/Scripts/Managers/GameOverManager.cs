﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
  public float RestartTime = 5f;

  private Animator _animator;
  private float _restartTimer;
  private bool _isGameOver;

  private void Awake()
  {
    _animator = GetComponent<Animator>();

    FindObjectOfType<ScoreManager>().OnGameOver += OnGameOver;
  }

  private void OnGameOver()
  {
    //Time.timeScale = 0;

    _isGameOver = true;
    
    _animator.SetTrigger("GameOver");
  }

  private void Update()
  {
    if (!_isGameOver)
      return;

    _restartTimer += Time.deltaTime;

    if (_restartTimer >= RestartTime)
    {
      SceneManager.LoadScene("Menu");
    }
  }
}
