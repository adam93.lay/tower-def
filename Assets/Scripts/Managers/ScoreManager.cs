﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
  public int Money { get; private set; }
  public int Lives { get; private set; }

  public Action OnGameOver;
  public Action<int> OnLivesChanged;
  public Action<int> OnMoneyChanged;

  private void Start()
  {
    AddMoney(50);
    Lives = 10;

    OnLivesChanged?.Invoke(Lives);
  }

  public void AddMoney(int money)
  {
    Money += money;

    OnMoneyChanged?.Invoke(Money);
  }

  public bool SpendMoney(int money)
  {
    if (Money - money < 0)
      return false;

    Money -= money;

    OnMoneyChanged?.Invoke(Money);

    return true;
  }

  public void DecrementLives(int lives)
  {
    Lives -= lives;

    OnLivesChanged?.Invoke(Lives);

    if (Lives <= 0)
      OnGameOver?.Invoke();
  }
}
