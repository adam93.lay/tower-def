﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Managers
{
  public class InterfaceManager : MonoBehaviour
  {
    public Text MoneyText;
    public Text LivesText;
    public Text ScoreText;
    public Text WaveText;
    public Text AlertText;
    public RectTransform WaveProgressBar;

    public GameObject TurretUpgradePanel;
    public Text TurretInfoText;
    public Text UpgradeButtonText;

    public GameObject RangeIndicatorPrefab;

    private ScoreManager _scoreManager;
    private EnemyManager _enemyManager;
    private TurretManager _turretManager;

    private float waveBarWidth = 200f;
    private Dictionary<string, Sprite> _allSprites;

    private readonly List<GameObject> _rangeIndicators = new List<GameObject>();

    private void Awake()
    {
      _allSprites = Resources.LoadAll<Sprite>("Images")
        .ToDictionary(k => k.name, v => v);

      _enemyManager = FindObjectOfType<EnemyManager>();
      _turretManager = FindObjectOfType<TurretManager>();
      _scoreManager = FindObjectOfType<ScoreManager>();

      _enemyManager.OnWaveChanged += (current, total) =>
      {
        string waveText = current + " / " + total;
        SetWaveText(waveText);
        ShowMessage("New wave! " + waveText);
      };
      _enemyManager.OnWaveComplete += (wave) =>
      {
        ShowMessage($"Wave {wave} complete!");
        WaveProgressBar.sizeDelta = new Vector2(waveBarWidth, WaveProgressBar.rect.height);
      };
      _enemyManager.OnAllWavesCompleted += () =>
      {
        StopCoroutine(nameof(ShowAlert));
        StartCoroutine(ShowAlert("Victory! All waves completed!"));
      };
      _enemyManager.OnWaveProgress += (remain, total) =>
      {
        float prc = (float)remain / (float)total;

        WaveProgressBar.sizeDelta = new Vector2(waveBarWidth * prc, WaveProgressBar.rect.height);
      };

      _scoreManager.OnMoneyChanged += money => SetMoneyText(money.ToString("N0"));
      _scoreManager.OnLivesChanged += lives => SetLivesText(lives.ToString());

      Turret.OnChanged += turret =>
      {
        SetTurretInfo(turret);
        ShowRangeIndicator(turret);
      };

      MouseManager.OnPositionSelected += pos =>
      {
        GameObject turretGo = _turretManager.GetTurret(pos);

        RemoveRangeIndicators();

        if (turretGo != null)
        {
          var turret = turretGo.GetComponentInChildren<Turret>();

          // Open upgrade panel
          TurretUpgradePanel.SetActive(true);

          ShowRangeIndicator(turret);
          SetTurretInfo(turret);
        }
        else
        {
          // Close upgrade panel
          TurretUpgradePanel.SetActive(false);
        }
      };
    }

    public void ShowMessage(string message)
    {
      StopCoroutine(nameof(ShowAlert));
      StartCoroutine(ShowAlert(message));
    }

    private void RemoveRangeIndicators()
    {
      // Remove existing indicators
      for (int i = _rangeIndicators.Count - 1; i >= 0; i--)
      {
        Destroy(_rangeIndicators[i]);
        _rangeIndicators.RemoveAt(i);
      }
    }

    public void ToggleSpeed()
    {
      Time.timeScale = Time.timeScale > 1 ? 1 : 3;
    }

    private void ShowRangeIndicator(Turret turret)
    {
      RemoveRangeIndicators();

      GameObject ri = Instantiate(RangeIndicatorPrefab, turret.transform);

      ri.transform.localScale = Vector2.one * (turret.Range * 2f);

      _rangeIndicators.Add(ri);
    }

    private IEnumerator ShowAlert(string text)
    {
      AlertText.transform.gameObject.SetActive(true);
      AlertText.text = text;
      AlertText.color = new Color(1, 1, 1, 0);

      float fadeTime = 0.1f;

      for (float i = 0; i <= fadeTime; i += Time.deltaTime)
      {
        // set color with i as alpha
        AlertText.color = new Color(1, 1, 1, i * (1 / fadeTime));
        yield return null;
      }

      yield return new WaitForSeconds(2.5f);

      for (float i = fadeTime; i >= 0; i -= Time.deltaTime)
      {
        // set color with i as alpha
        AlertText.color = new Color(1, 1, 1, i * (1 / fadeTime));
        yield return null;
      }

      AlertText.transform.gameObject.SetActive(false);
      AlertText.color = new Color(1, 1, 1, 0);
      AlertText.text = "";
    }

    public Sprite GetSpriteByName(string name)
    {
      // TODO: Error handling
      return _allSprites[name];
    }

    private void SetTurretInfo(Turret turret)
    {
      TurretInfoText.text = $"{turret.name} (lvl {turret.UpgradeLevel}){Environment.NewLine}DPS {turret.Damage / turret.FireRate}{Environment.NewLine}Range {turret.Range}";
      UpgradeButtonText.text = "Upgrade - " + turret.Config.Cost;
    }

    public void SetMoneyText(string text)
    {
      MoneyText.text = text;
    }

    public void SetLivesText(string text)
    {
      LivesText.text = text;
    }

    public void SetScoreText(string text)
    {
      ScoreText.text = text;
    }

    public void SetWaveText(string text)
    {
      WaveText.text = text;
    }
  }
}
