﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Assets.Scripts.Enemies;
using Assets.Scripts.StateMachines;
using Assets.Scripts.Turrets;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
  public GameObject Spawn;
  public GameObject Destination;
  public GameObject EnemyPrefab;

  /// <summary>
  /// Current wave, total waves
  /// </summary>
  public Action<int, int> OnWaveChanged;
  public Action<int, int> OnWaveProgress;
  public Action<int> OnWaveComplete;
  public Action OnAllWavesCompleted;
  public List<Wave> Waves { get; private set; }

  private ScoreManager _scoreManager;
  private List<GameObject> _enemies;
  private Dictionary<string, EnemyConfig> _enemyConfigs;

  private void Start()
  {
    _enemyConfigs = Resources.LoadAll<EnemyConfig>("Config/Enemies")
      .ToDictionary(k => k.name, v => v);
    
    _enemies = new List<GameObject>();
    Waves = new List<Wave>
    {
      new Wave(new []{ "Minion_Group", "Minion_Group" }, 2f),
      new Wave(new []{ "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion" }),
      new Wave(new []{ "Soldier", "Soldier", "Soldier", "Soldier", "Soldier", "Soldier" }),
      new Wave(new []{ "Minion", "Minion", "Soldier", "Minion", "Minion", "Soldier", "Minion", "Minion", "Soldier", "Minion", "Minion", "Soldier" }),
      new Wave(new []{ "Soldier", "Soldier", "Minion", "Soldier", "Soldier", "Minion", "Soldier", "Soldier", "Minion", "Soldier", "Soldier", "Minion" }),
      new Wave(new []{ "Soldier", "Soldier", "Boss", "Soldier", "Soldier" }),
      new Wave(new []{ "Soldier", "Soldier", "Soldier", "Soldier", "Soldier", "Soldier", "Soldier", "Soldier", "Soldier", "Soldier", "Soldier", "Soldier"}),
      new Wave(new []{ "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion"}),
      new Wave(new []{ "Boss", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion", "Minion" }),
      new Wave(new []{ "Minion", "Minion", "Soldier", "Soldier", "Minion", "Minion", "Soldier", "Soldier", "Minion", "Minion", "Soldier", "Soldier", "Boss" }),
      new Wave(new []{ "Boss", "Minion", "Minion", "Soldier", "Boss", "Minion", "Minion", "Soldier" }),
    };
    _stateMachine = new StateMachine<EnemyManager>(this);
    _stateMachine.ChangeState(new IdleState());
    _scoreManager = FindObjectOfType<ScoreManager>();
  }

  #region Spawning

  private void SpawnEnemy(string enemyType)
  {
    GameObject enemyGo = Instantiate(EnemyPrefab, Spawn.transform.position, Quaternion.identity);
    enemyGo.name = enemyType;
    enemyGo.GetComponent<Enemy>().Config = GetEnemyConfig(enemyType);
    enemyGo.transform.SetParent(this.transform);

    var enemy = enemyGo.GetComponent<Enemy>();
    enemy.Target = Destination.transform;
    float mult = (CurrentWaveId - 1) * 0.7f;
    enemy.HealthMultiplier = 1f + mult;

    //print($"Enemy: {enemyType} Max Health: {enemy.MaxHealth} Health Mult: {enemy.HealthMultiplier}");

    _enemies.Add(enemyGo);
  }

  private IEnumerator SpawnGroup(string enemyType)
  {
    for (int i = 0; i < 4; i++)
    {
      SpawnEnemy(enemyType);

      yield return new WaitForSeconds(0.1f);
    }
  }

  public void ReachedTarget(GameObject enemy)
  {
    EnemyConfig config = GetEnemyConfig(enemy.name);

    // Remove lives from the player as the enemy reached destination
    _scoreManager.DecrementLives(config.LivesCost);

    RemoveEnemy(enemy);
  }

  public void Kill(GameObject enemy)
  {
    EnemyConfig config = GetEnemyConfig(enemy.name);

    // Reward the player with the money value for the killed enemy
    _scoreManager.AddMoney(config.MoneyValue);

    RemoveEnemy(enemy);
  }

  private void RemoveEnemy(GameObject enemy)
  {
    // Remove enemy from manager's list to stop tracking it
    _enemies.Remove(enemy);

    if (_enemies.Count == 0 && CurrentWave.EnemiesRemaining == 0)
    {
      // If no enemies are being tracked and no enemies left in the wave then the wave is complete
      OnWaveComplete?.Invoke(CurrentWaveId);

      if (CurrentWave == Waves.Last())
      {
        OnAllWavesCompleted?.Invoke();
      }
    }

    Destroy(enemy);
  }

  #endregion

  public GameObject GetEnemyClosestTo(Vector2 position)
  {
    float closestDiff = Mathf.Infinity;
    GameObject closestEnemy = null;

    foreach (var enemy in _enemies)
    {
      Vector2 enemyPos = enemy.transform.position;
      Vector2 diff = position - enemyPos;

      if (diff.magnitude < closestDiff)
      {
        closestDiff = diff.magnitude;
        closestEnemy = enemy;
      }
    }

    return closestEnemy;
  }

  public List<GameObject> GetEnemiesWithinRange(Vector2 position, float range)
  {
    var inRange = new List<GameObject>();

    foreach (var enemy in _enemies)
    {
      Vector2 enemyPos = enemy.transform.position;
      Vector2 diff = position - enemyPos;

      if (diff.magnitude <= range)
      {
        inRange.Add(enemy);
      }
    }

    return inRange;
  }

  private EnemyConfig GetEnemyConfig(string type)
  {
    return _enemyConfigs[type];
  }

  #region Waves

  public int CurrentWaveId { get; private set; }
  public Wave CurrentWave { get; private set; }

  public void NextWave()
  {
    if (CurrentWaveId + 1 > Waves.Count || _stateMachine.CurrentState is InWaveState)
      return;

    _stateMachine.ChangeState(new InWaveState());
  }

  private IEnumerator SpawnWave(Wave wave)
  {
    for (int i = 0; i < wave.TotalEnemies; i++)
    {
      string nextEnemyType = wave.Enemies[wave.Enemies.Length - wave.EnemiesRemaining];

      if (nextEnemyType.EndsWith("_Group"))
      {
        StartCoroutine(SpawnGroup(nextEnemyType.Replace("_Group", "")));
      }
      else
      {
        SpawnEnemy(nextEnemyType);
      }
      
      wave.EnemiesRemaining--;
      OnWaveProgress?.Invoke(wave.EnemiesRemaining, wave.TotalEnemies);
      yield return new WaitForSeconds(wave.SpawnTime);
    }

    _stateMachine.ChangeState(new IdleState());
  }

  #endregion

  #region States

  private StateMachine<EnemyManager> _stateMachine;

  private class IdleState : State<EnemyManager> { }

  private class InWaveState : State<EnemyManager>
  {
    public override void EnterState(EnemyManager owner)
    {
      owner.CurrentWave = owner.Waves[owner.CurrentWaveId];
      owner.CurrentWaveId++;

      owner.StartCoroutine(owner.SpawnWave(owner.CurrentWave));
      owner.OnWaveChanged?.Invoke(owner.CurrentWaveId, owner.Waves.Count);
    }
  }

  #endregion
}
