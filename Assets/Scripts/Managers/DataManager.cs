﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Data;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Scripts.Managers
{
  public class DataManager : MonoBehaviour
  {
    private void Awake()
    {
      DontDestroyOnLoad(this.gameObject);
      LoadGlobalSettings();
    }

    private static string GetPath()
    {
      return (Application.persistentDataPath + "/GlobalSettings.json")
        .Replace('\\', Path.DirectorySeparatorChar);
    }

    public static void SaveGlobalSettings()
    {
      string fileContent = JsonConvert.SerializeObject(GlobalSettings.Instance);
      string filePath = GetPath();

      File.WriteAllText(filePath, fileContent);
    }

    public static void LoadGlobalSettings()
    {
      string filePath = GetPath();

      if (!File.Exists(filePath))
      {
        GlobalSettings.LoadInstance(new GlobalSettings());
        return;
      }

      string fileContents = File.ReadAllText(filePath);

      var settings = JsonConvert.DeserializeObject<GlobalSettings>(fileContents);

      GlobalSettings.LoadInstance(settings);
    }
  }
}
