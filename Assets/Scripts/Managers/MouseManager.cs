﻿using System;
using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.UIElements;

public class MouseManager : MonoBehaviour
{
  public GameObject SelectionCursor;

  private Vector2 _mousePosition;
  private BuildManager _buildManager;

  public static Vector2? SelectedPosition { get; private set; }

  public static Action<Vector2> OnPositionSelected;

  private void Start()
  {
    _buildManager = FindObjectOfType<BuildManager>();
  }

  private void Update()
  {
    if (EventSystem.current.IsPointerOverGameObject())
      return;

    _mousePosition = GetMousePosition();

    bool leftUp = Input.GetMouseButtonUp((int)MouseButton.LeftMouse);
    bool rightUp = Input.GetMouseButtonUp((int)MouseButton.RightMouse);
    
    if (leftUp || rightUp)
    {
      SelectedPosition = new Vector2(Mathf.RoundToInt(_mousePosition.x), Mathf.RoundToInt(_mousePosition.y));

      SelectionCursor.transform.position = SelectedPosition.Value;
      
      // For test - quicker to build
      if (rightUp)
        _buildManager.BuildTurret("Basic");

      OnPositionSelected?.Invoke(SelectedPosition.Value);
    }
  }

  private Vector2 GetMousePosition()
  {
    Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

    return pos;
  }
}
