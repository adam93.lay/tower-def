﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.Scripts.Managers;
using Assets.Scripts.Turrets;
using UnityEngine;

public class Turret : MonoBehaviour
{
  public TurretConfig Config;
  public GameObject BulletPrefab;

  public float FireRate => Config.BaseFireRate;
  public float Range => Config.BaseRange + (UpgradeLevel - 1);
  public float Damage => UpgradeLevel * Config.BaseDamage;
  public int UpgradeLevel { get; private set; } = 1;
  
  private EnemyManager _enemyManager;
  private InterfaceManager _interfaceManager;

  private Transform _turret;
  private SpriteRenderer _upgradeIconSpriteRenderer;
  private GameObject _currentTarget;
  private float _fireCooldown = 0f;

  public static Action<Turret> OnChanged;

  private void Start()
  {
    _enemyManager = FindObjectOfType<EnemyManager>();
    _interfaceManager = FindObjectOfType<InterfaceManager>();

    _turret = transform.GetChild(1); // Will always be 2nd child
    var sr = _turret.GetComponent<SpriteRenderer>();
    sr.sprite = Config.Sprite;

    _upgradeIconSpriteRenderer = transform.GetChild(2) // Will always be 3rd child
      .GetComponent<SpriteRenderer>();
  }

  private void Update()
  {
    // Try and find a target...
    _currentTarget = TargetEnemy();

    // ...stop if no enemies within range
    if (_currentTarget == null)
      return;

    // Visually rotate turret to point at target
    FaceTarget();

    // Fire!
    ShootAtTarget();
  }

  public void Upgrade()
  {
    if (UpgradeLevel < Config.UpgradeLevels)
    {
      UpgradeLevel += 1;
      OnChanged?.Invoke(this);

      string newSpriteName = "Upgrade_" + (UpgradeLevel - 1);

      _upgradeIconSpriteRenderer.sprite = _interfaceManager.GetSpriteByName(newSpriteName);
    }
  }

  private void ShootAtTarget()
  {
    _fireCooldown -= Time.deltaTime;

    if (_fireCooldown > 0)
      return;

    GameObject bulletGo = Instantiate(BulletPrefab, this.transform);
    var bullet = bulletGo.GetComponent<Bullet>();

    bullet.Config = Config.BulletConfig;
    bullet.Target = _currentTarget;
    bullet.Damage = Damage;

    _fireCooldown = Config.BaseFireRate;
  }

  private GameObject TargetEnemy()
  {
    // If we're already targetting an emeny and it's still in range, don't change target
    if (_currentTarget != null && IsInRange(_currentTarget))
      return _currentTarget;

    GameObject enemy = _enemyManager.GetEnemyClosestTo(transform.position);

    // If there was no enemy or the closest enemy is out of range
    if (enemy == null || !IsInRange(enemy))
      return null;

    return enemy;
  }

  private bool _isTurningToFace = false;

  private void FaceTarget()
  {
    if (!_isTurningToFace)
      StartCoroutine(TurnToFace(_currentTarget.transform.position, 10));
  }

  private IEnumerator TurnToFace(Vector2 targetPos, float speed)
  {
    _isTurningToFace = true;
    Quaternion start = _turret.rotation;
    Quaternion target = ((Vector2)_turret.position).Face(targetPos, -90f);

    for (float i = 0; i <= 1; i += speed * Time.deltaTime)
    {
      _turret.rotation = Quaternion.Slerp(start, target, i);

      yield return null;
    }

    _isTurningToFace = false;
  }

  private bool IsInRange(GameObject enemy)
  {
    Vector3 vectorToEnemy = transform.position - enemy.transform.position;

    return vectorToEnemy.magnitude < Range;
  }
}
