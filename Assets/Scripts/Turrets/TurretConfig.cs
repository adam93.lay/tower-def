﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Bullets;
using UnityEngine;

namespace Assets.Scripts.Turrets
{
  [CreateAssetMenu(fileName = "TurretConfig", menuName = "Tower Defense/TurretConfig", order = 1)]
  public class TurretConfig : ScriptableObject
  {
    public string Name;
    public int Cost;
    public float BaseFireRate;
    public float BaseDamage;
    public float BaseRange;
    public int UpgradeLevels;
    public BulletConfig BulletConfig;
    public Sprite Sprite;
  }
}
