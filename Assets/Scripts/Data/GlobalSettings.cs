﻿using System.Collections.Generic;

namespace Assets.Scripts.Data
{
  public class GlobalSettings
  {
    public static GlobalSettings Instance { get; private set; }

    public int GlobalMoney { get; private set; }
    public Dictionary<TurretTypes, int> TurretLevels { get; private set; }

    public GlobalSettings()
    {
      TurretLevels = new Dictionary<TurretTypes, int> { [TurretTypes.Basic] = 2 };
    }

    public static void LoadInstance(GlobalSettings instance)
    {
      Instance = instance;
    }
  }
}
