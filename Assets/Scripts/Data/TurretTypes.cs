﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Data
{
  public enum TurretTypes
  {
    Basic,
    Missile
  }

  public static class TurretTypesExtensions
  {
    public static string GetName(this TurretTypes type)
    {
      switch (type)
      {
        case TurretTypes.Basic: return "Basic";
        case TurretTypes.Missile: return "Missile";
      }

      throw new ArgumentOutOfRangeException(nameof(type), "TurretTypes." + type + " does not have a name?");
    }
  }
}
