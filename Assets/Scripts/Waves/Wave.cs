﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
  public class Wave
  {
    public int TotalEnemies { get; }
    public int EnemiesRemaining { get; set; }
    public float SpawnTime { get; }
    public string[] Enemies { get; }

    public Wave(string[] enemies, float spawnTime = 1.5f)
    {
      TotalEnemies = enemies.Length;
      EnemiesRemaining = enemies.Length;
      Enemies = enemies;
      SpawnTime = spawnTime;
    }
  }
}
