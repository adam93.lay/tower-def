﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Assets.Scripts.Enemies;
using Assets.Scripts.Pathfinding;
using Assets.Scripts.StateMachines;
using Assets.Scripts.Turrets;
using Pathfinding.Astar;
using UnityEngine;

public class Enemy : MonoBehaviour
{
  public Transform Target;
  public EnemyConfig Config;
  
  public float Health { get; private set; }
  public float HealthMultiplier { get; set; }
  public float MaxHealth => Config.BaseHealth * HealthMultiplier;

  private Waypoint[] _path;
  private EnemyManager _enemyManager;
  private Grid _grid;

  private void Start()
  {
    _grid = FindObjectOfType<Grid>();
    _enemyManager = FindObjectOfType<EnemyManager>();

    Health = MaxHealth;

    var sr = GetComponent<SpriteRenderer>();
    sr.sprite = Config.Sprite;

    RequestPath();

    Grid.OnGridRefreshed += RequestPath;
  }

  private void OnDestroy()
  {
    Grid.OnGridRefreshed -= RequestPath;
  }

  private void RequestPath()
  {
    Pathfinder.RequestPath(transform.position, Target.position, OnPathFound);
  }

  private void OnPathFound(PathResult result)
  {
    StopCoroutine("FollowPath");

    if (result.Success && result.Path != null && result.Path.Length > 0)
    {
      _path = ProcessPath(result.Path);
      StartCoroutine("FollowPath");
    }
    else
    {
      print("Finding path failed from: " + transform.position);
      _path = null;
    }
  }

  private Waypoint[] ProcessPath(Vector2[] path)
  {
    var waypoints = new Waypoint[path.Length];

    Vector2 from = transform.position;
    Vector2 to = path[0];

    waypoints[0] = new Waypoint(from, to, _grid);

    for (int i = 1; i < path.Length; i++)
    {
      waypoints[i] = new Waypoint(path[i - 1], path[i], _grid);
    }

    return waypoints;
  }

  private IEnumerator FollowPath()
  {
    if (_path == null || _path.Length < 1)
    {
      print("Path is empty");
      yield break;
    }

    int targetIndex = 0;
    float bezierTime = 0;
    float rotationTime = 0;
    Quaternion startRotation = transform.rotation;
    Waypoint waypoint = _path[0];

    while (targetIndex < _path.Length)
    {
      if ((Vector2)transform.position == waypoint.To)
      {
        targetIndex++;

        if (targetIndex >= _path.Length)
          break;

        bezierTime = 0;
        rotationTime = 0;
        startRotation = transform.rotation;
        waypoint = _path[targetIndex];
      }

      float t = Config.Speed * Time.deltaTime;
      
      if (waypoint.TravelViaBezier)
      {
        rotationTime += t;
        Quaternion targetRotation = ((Vector2)transform.position).Face(waypoint.To);
        transform.rotation = Quaternion.Slerp(startRotation, targetRotation, rotationTime);
        bezierTime = Mathf.Clamp01(bezierTime + t / 1.571f);
        transform.position = waypoint.From.MoveThroughBezierTo(waypoint.BezierMidpoint.Value, waypoint.To, bezierTime);
      }
      else
      {
        rotationTime += Time.deltaTime * 10;
        Quaternion targetRotation = ((Vector2)transform.position).Face(waypoint.To);
        transform.rotation = Quaternion.Slerp(startRotation, targetRotation, rotationTime);
        transform.position = Vector2.MoveTowards(transform.position, waypoint.To, t);
      }

      yield return null;
    }

    // Reached destination
    _enemyManager.ReachedTarget(this.gameObject);
  }

  public void TakeDamage(float damage)
  {
    Health -= damage;

    if (Health <= 0)
      _enemyManager.Kill(this.gameObject);
  }

  private void OnDrawGizmos()
  {
    if (_path != null)
    {
      for (int i = 0; i < _path.Length - 1; i++)
      {
        //Gizmos.color = Color.black;
        //Gizmos.DrawCube(_path[i], Vector3.one * 0.15f);
        //Gizmos.DrawLine(_path[i], _path[i + 1]);
      }
    }
  }
}
