﻿using UnityEngine;

namespace Assets.Scripts.Enemies
{
  [CreateAssetMenu(fileName = "EnemyConfig", menuName = "Tower Defense/EnemyConfig", order = 2)]
  public class EnemyConfig : ScriptableObject
  {
    public string Name;
    public int LivesCost;
    public int MoneyValue;
    public float Speed;
    public float BaseHealth;
    public Sprite Sprite;
  }
}
