﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Assets.Scripts.Bullets;
using UnityEngine;

public class Bullet : MonoBehaviour
{
  public GameObject Target;
  public BulletConfig Config;

  public float Damage { get; set; }

  private void Start()
  {
    var sr = GetComponent<SpriteRenderer>();
    sr.sprite = Config.Sprite;
  }

  private void Update()
  {
    if (Target == null)
    {
      Destroy(this.gameObject);
      return;
    }

    // Rotate to face towards target
    transform.rotation = ((Vector2) transform.position).Face(Target.transform.position, -90);

    float deltaSpeed = Config.Speed * Time.deltaTime;
    Vector2 distance = Target.transform.position - transform.position;

    if (distance.magnitude < deltaSpeed)
    {
      // Reached target
      if (Config.Area > 0)
      {
        IEnumerable<Enemy> targets = FindObjectOfType<EnemyManager>()
          .GetEnemiesWithinRange(transform.position, Config.Area)
          .Select(e => e.GetComponent<Enemy>());

        foreach (Enemy enemy in targets)
          enemy.TakeDamage(Damage);
      }
      else
      {
        Target.GetComponent<Enemy>().TakeDamage(Damage);
      }

      StartCoroutine(Explode());
    }
    else
    {
      // Move towards target
      transform.position += (Target.transform.position - transform.position).normalized * deltaSpeed;
    }
  }

  private IEnumerator Explode()
  {
    Vector3 startScale = transform.localScale;
    Vector3 targetScale = transform.localScale + Vector3.one;

    for (float t = 0; t < 1; t += Time.deltaTime)
    {
      this.transform.localScale = Vector3.Lerp(startScale, targetScale, t);

      yield return null;
    }

    Destroy(this.gameObject);
  }
}
