﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Bullets
{
  [CreateAssetMenu(fileName = "BulletConfig", menuName = "Tower Defense/BulletConfig", order = 5)]
  public class BulletConfig : ScriptableObject
  {
    public float Speed = 4f;
    public float Area = 0f;
    public Sprite Sprite;
  }
}
