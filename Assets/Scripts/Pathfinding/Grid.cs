﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml;
using UnityEngine;
using UnityEngine.VR;

namespace Pathfinding.Astar
{
  public class Grid : MonoBehaviour
  {
    public Node[,] Nodes { get; set; }
    public List<Node> AllNodes { get; private set; }
    public Vector2 WorldSize;
    public float NodeRadius;
    public LayerMask UnwalkableMask;

    private float _nodeDiameter;
    private int gridSizeX;
    private int gridSizeY;

    public static Action OnGridRefreshed;

    public int MaxSize => gridSizeX * gridSizeY;

    private void Awake()
    {
      _nodeDiameter = NodeRadius * 2;
      gridSizeX = Mathf.RoundToInt(WorldSize.x / _nodeDiameter);
      gridSizeY = Mathf.RoundToInt(WorldSize.y / _nodeDiameter);

      CreateGrid();
    }

    public void CreateGrid()
    {
      AllNodes = new List<Node>();
      Nodes = new Node[gridSizeX, gridSizeY];
      Vector2 worldBottomLeft = ((Vector2)transform.position) - Vector2.right * WorldSize.x / 2 - Vector2.up * WorldSize.y / 2;

      for (int x = 0; x < gridSizeX; x++)
        for (int y = 0; y < gridSizeY; y++)
        {
          Vector2 worldPoint = worldBottomLeft
            + Vector2.right * (x * _nodeDiameter + NodeRadius)
            + Vector2.up * (y * _nodeDiameter + NodeRadius);

          bool walkable = !Physics2D.OverlapCircle(worldPoint, NodeRadius / 2, UnwalkableMask);

          AllNodes.Add(Nodes[x, y] = new Node(x, y, walkable, worldPoint));
        }

      OnGridRefreshed?.Invoke();
    }

    public Node GetNodeFromWorldPoint(Vector2 worldPosition)
    {
      //var pos = worldPosition - (Vector2)transform.position;
      ////float prcX = Mathf.Clamp01(worldPosition.x / WorldSize.x + 0.5f); 
      ////float prcY = Mathf.Clamp01(worldPosition.y / WorldSize.y + 0.5f);

      //float prcX = Mathf.Clamp01((pos.x + WorldSize.x / 2) / WorldSize.x);
      //float prcY = Mathf.Clamp01((pos.y + WorldSize.y / 2) / WorldSize.y);

      //int x = Mathf.RoundToInt((gridSizeX - 1) * prcX);
      //int y = Mathf.RoundToInt((gridSizeY - 1) * prcY);

      int x = Mathf.RoundToInt(worldPosition.x);
      int y = Mathf.RoundToInt(worldPosition.y);

      if (x < 0 || y < 0 || x > Nodes.GetUpperBound(0) || y > Nodes.GetUpperBound(1))
        return null;

      return Nodes[x, y];
    }

    public bool IsNodeWalkable(Vector2 worldPosition)
    {
      return GetNodeFromWorldPoint(worldPosition).Walkable;
    }

    public List<Node> GetNeighbours(Node node, bool allowDiag)
    {
      var neighbours = new List<Node>();

      for (int x = -1; x <= 1; x++)
        for (int y = -1; y <= 1; y++)
        {
          if (x == 0 && y == 0)
            continue;

          int xPos = node.GridX + x;
          int yPos = node.GridY + y;

          if (!allowDiag && xPos != node.GridX && yPos != node.GridY)
            continue;

          if (xPos >= 0 && xPos < gridSizeX)
            if (yPos >= 0 && yPos < gridSizeY)
            {
              neighbours.Add(Nodes[xPos, yPos]);
            }
        }

      return neighbours;
    }

    private void OnDrawGizmos()
    {
      Gizmos.DrawWireCube(transform.position, new Vector3(WorldSize.x, WorldSize.y, -1));

      if (Nodes != null)
      {
        foreach (Node node in Nodes)
        {
          Gizmos.color = node.Walkable ? Color.white : Color.red;
          Gizmos.DrawSphere(node.Position, NodeRadius * 0.3f);
        }
      }
    }
  }
}
