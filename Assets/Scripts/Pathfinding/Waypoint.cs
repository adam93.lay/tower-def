﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pathfinding.Astar;
using UnityEngine;

namespace Assets.Scripts.Pathfinding
{
  public class Waypoint
  {
    public Vector2 From { get; set; }
    public Vector2 To { get; set; }
    public bool TravelViaBezier { get; set; }
    public Vector2? BezierMidpoint{ get; set; }

    public Waypoint(Vector2 from, Vector2 to, Grid grid)
    {
      From = from;
      To = to;
      
      Vector2 diff = From - To;
      bool isDiagonal = Mathf.Abs(diff.x) == Mathf.Abs(diff.y);

      // If it's not diagonal then we won't need to work out a bezier curve around an unwalkable node
      if (!isDiagonal)
        return;

      // If we are moving diagonally, then check both squares on each side of the diagonal line
      Node check1 = grid.GetNodeFromWorldPoint(new Vector2(To.x + diff.x, To.y));
      Node check2 = grid.GetNodeFromWorldPoint(new Vector2(To.x, To.y + diff.y));

      // If either of the nodes next to our diagonal line are unwalkable,
      // then we need to walk around them via a bezier
      TravelViaBezier = !check1.Walkable || !check2.Walkable;

      // If we're walking straight then we don't need to a find a bezier curve midpoint
      if (!TravelViaBezier)
        return;

      // Since we can't move diagonally between two unwalkable tiles,
      // then use the first walkable node's position
      BezierMidpoint = check1.Walkable ? check1.Position : check2.Position;
    }
  }
}
