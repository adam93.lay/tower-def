﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Pathfinding
{
  public class PathResult
  {
    public bool Success { get; set; }
    public Vector2[] Path { get; set; }
  }
}
