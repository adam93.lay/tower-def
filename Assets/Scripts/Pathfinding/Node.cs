﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Pathfinding;
using UnityEngine;

namespace Pathfinding.Astar
{
  public class Node : IHeapItem<Node>
  {
    public bool Walkable { get; private set; }
    public Vector2 Position { get; private set; }

    public int GridX { get; set; }
    public int GridY { get; set; }

    public Node Parent;

    public int G;
    public int H;
    public int F => G + H;

    public Node(int gridX, int gridY, bool walkable, Vector2 position)
    {
      GridX = gridX;
      GridY = gridY;

      Walkable = walkable;
      Position = position;
    }

    #region IHeapItem

    public int HeapIndex { get; set; }

    public int CompareTo(Node other)
    {
      int comparison = F.CompareTo(other.F);
      if (comparison == 0)
        comparison = H.CompareTo(other.H);
      return -comparison;
    }

    #endregion
  }
}
