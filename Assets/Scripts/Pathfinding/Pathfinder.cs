﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Assets.Scripts.Pathfinding;
using UnityEngine;

namespace Pathfinding.Astar
{
  public class Pathfinder : MonoBehaviour
  {
    private Grid _grid;
    private static Pathfinder _instance;

    private void Awake()
    {
      _instance = this;

      _grid = FindObjectOfType<Grid>();
    }

    public static void RequestPath(Vector2 start, Vector2 destination, Action<PathResult> callback)
    {
      callback(_instance.FindPath(start, destination));
    }

    public PathResult FindPath(Vector2 start, Vector2 destination, Vector2? unwalkableOverride = null)
    {
      Node startNode = _grid.GetNodeFromWorldPoint(start);
      Node destNode = _grid.GetNodeFromWorldPoint(destination);

      bool pathSuccess = false;

      if (!startNode.Walkable || !destNode.Walkable)
        return new PathResult { Success = false };

      var openSet = new Heap<Node>(_grid.MaxSize);
      var closedSet = new HashSet<Node>();

      openSet.Add(startNode);

      while (openSet.Count > 0)
      {
        Node currentNode = openSet.RemoveFirst();

        if (currentNode == destNode)
        {
          pathSuccess = true;
          break;
        }

        closedSet.Add(currentNode);

        foreach (Node neighbour in _grid.GetNeighbours(currentNode, true))
        {
          if (!neighbour.Walkable || closedSet.Contains(neighbour))
            continue;

          if (unwalkableOverride.HasValue && neighbour.Position == unwalkableOverride)
            continue;

          int movementCostToNeighbour = currentNode.G + DistBetween(currentNode, neighbour);
          if (movementCostToNeighbour < neighbour.G || !openSet.Contains(neighbour))
          {
            neighbour.G = movementCostToNeighbour;
            neighbour.H = DistBetween(neighbour, destNode);
            neighbour.Parent = currentNode;

            if (!openSet.Contains(neighbour))
              openSet.Add(neighbour);
            else
              openSet.UpdateItem(neighbour);
          }
        }
      }

      return new PathResult
      {
        Success = pathSuccess,
        Path = pathSuccess ? RetracePath(startNode, destNode) : null
      };
    }

    //public bool FindPath_old(Vector2 start, Vector2 destination, out Vector2[] waypoints)
    //{
    //  Node startNode = _grid.GetNodeFromWorldPoint(start);
    //  Node destNode = _grid.GetNodeFromWorldPoint(destination);

    //  waypoints = null;
    //  bool pathSuccess = false;

    //  if (startNode.Walkable && destNode.Walkable)
    //    return false;

    //  var openSet = new Heap<Node>(_grid.MaxSize);
    //  var closedSet = new HashSet<Node>();

    //  openSet.Add(startNode);

    //  var gScore = _grid.AllNodes.ToDictionary(k => k, v => Mathf.Infinity); //new Dictionary<Node, float> { [startNode] = 0 };
    //  gScore[startNode] = 0;
    //  var fScore = _grid.AllNodes.ToDictionary(k => k, v => Mathf.Infinity);
    //  fScore[startnode] = 


    //  while (openSet.Count > 0)
    //  {
    //    Node currentNode = openSet.RemoveFirst();

    //    closedSet.Add(currentNode);

    //    if (currentNode == destNode)
    //    {
    //      pathSuccess = true;
    //      break;
    //    }

    //    foreach (Node neighbour in _grid.GetNeighbours(currentNode))
    //    {
    //      if (!neighbour.Walkable || closedSet.Contains(neighbour))
    //        continue;

    //      float tentative_gScore = gScore[currentNode] + DistBetween(currentNode, neighbour);

    //      if (tentative_gScore >= gScore[neighbour])

    //      if (tentative_gScore < neighbour.G || !openSet.Contains(neighbour))
    //      {
    //        neighbour.G = tentative_gScore;
    //        neighbour.H = DistBetween(neighbour, destNode);
    //        neighbour.Parent = currentNode;

    //        if (!openSet.Contains(neighbour))
    //        {
    //          openSet.Add(neighbour);
    //        }
    //        else
    //        {
    //          openSet.UpdateItem(neighbour);
    //        }
    //      }
    //    }
    //  }

    //  if (pathSuccess)
    //  {
    //    waypoints = RetracePath(startNode, destNode);
    //  }

    //  return pathSuccess;
    //}

    private Vector2[] RetracePath(Node start, Node end)
    {
      var path = new List<Node>();
      Node currentNode = end;

      while (currentNode != start)
      {
        path.Add(currentNode);
        currentNode = currentNode.Parent;
      }
      // TODO: Fix simplifypath
      //Vector2[] waypoints = SimplifyPath(path);
      //Array.Reverse(waypoints);
      return path.Select(n => new Vector2(n.GridX, n.GridY)).Reverse().ToArray();
    }

    private Vector2[] SimplifyPath(List<Node> path)
    {
      var waypoints = new List<Vector2>();
      Vector2 directionOld = Vector2.zero;

      for (int i = 1; i < path.Count; i++)
      {
        var directionNew = new Vector2(path[i - 1].GridX - path[i].GridX, path[i - 1].GridY - path[i].GridY);

        if (directionNew != directionOld)
          waypoints.Add(path[i].Position);

        directionOld = directionNew;
      }

      return waypoints.ToArray();
    }

    private int DistBetween(Node from, Node to)
    {
      int dstX = Mathf.Abs(from.GridX - to.GridX);
      int dstY = Mathf.Abs(from.GridY - to.GridY);

      return dstX > dstY
        ? 14 * dstY + 10 * (dstX - dstY)
        : 14 * dstX + 10 * (dstY - dstX);
    }
  }
}
