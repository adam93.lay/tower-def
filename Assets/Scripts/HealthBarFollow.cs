﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarFollow : MonoBehaviour
{
  private Camera _camera;
  private Transform _enemyTransform;
  private Enemy _enemy;
  private RectTransform _innerRect;

  // Use this for initialization
  private void Start()
  {
    _camera = Camera.main;
    _enemyTransform = transform.parent.parent.transform;
    _enemy = _enemyTransform.GetComponent<Enemy>();
    _innerRect = transform.GetChild(0).GetComponent<RectTransform>();
  }

  // Update is called once per frame
  private void Update()
  {
    transform.parent.rotation = Quaternion.Euler(0, 0, _enemyTransform.rotation.z * -1f);

    float healthPrc = (_enemy.Health / _enemy.MaxHealth);// * 100f;

    _innerRect.sizeDelta = new Vector2(0.5f * healthPrc, _innerRect.rect.height);

    //_enemy.Health;
    //transform.position = _camera.WorldToScreenPoint(_enemyTransform.position);
  }
}
